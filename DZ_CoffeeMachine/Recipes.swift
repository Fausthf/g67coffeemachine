//
//  Recipes.swift
//  DZ_CoffeeMachine
//
//  Created by AV on 12/2/18.
//  Copyright © 2018 AV. All rights reserved.
//

import UIKit

class Recipes: NSObject {
    var drink = ""
    var waterNeeded = 0
    var coffeeNeeded = 0
    var milkNeeded = 0

    init(drink: String, waterNeeded: Int, coffeNeeded: Int, milkNeeded: Int){
        self.drink = drink
        self.waterNeeded = waterNeeded
        self.coffeeNeeded = coffeNeeded
        self.milkNeeded = milkNeeded
    }
    
}
