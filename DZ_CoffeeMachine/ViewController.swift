//
//  ViewController.swift
//  DZ_CoffeeMachine
//
//  Created by AV on 11/29/18.
//  Copyright © 2018 AV. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultMessageLabel: UILabel!
    @IBOutlet weak var waterLabel: UILabel!
    @IBOutlet weak var coffeeLabel: UILabel!
    @IBOutlet weak var milkLabel: UILabel!
    
    let coffeeMachine = CoffeeMachine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        refreshLabels()
        
    }

    func refreshLabels() {
        waterLabel.text = String(coffeeMachine.waterInMachine)
        coffeeLabel.text = String(coffeeMachine.coffeeInMachine)
        milkLabel.text = String(coffeeMachine.milkInMachine)
    }
    
    @IBAction func americanoButton(_ sender: UIButton) {
        resultMessageLabel.text = coffeeMachine.makeCoffe(choice: 0)
        refreshLabels()
    }
    
    @IBAction func capuccinoButton(_ sender: UIButton) {
        resultMessageLabel.text = coffeeMachine.makeCoffe(choice: 1)
        refreshLabels()
    }
    @IBAction func latteButton(_ sender: UIButton) {
        resultMessageLabel.text = coffeeMachine.makeCoffe(choice: 2)
        refreshLabels()
    }
    
    @IBAction func addBeansButton(_ sender: UIButton) {
        coffeeMachine.addToMachine(choice: 0)
        refreshLabels()
    }
    
    @IBAction func addWaterButton(_ sender: UIButton) {
        coffeeMachine.addToMachine(choice: 1)
        refreshLabels()
    }
    
    @IBAction func addMilkButton(_ sender: UIButton) {
        coffeeMachine.addToMachine(choice: 2)
        refreshLabels()
    }
    
}

