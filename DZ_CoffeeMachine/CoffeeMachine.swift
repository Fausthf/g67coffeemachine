//
//  CoffeeMachine.swift
//  DZ_CoffeeMachine
//
//  Created by AV on 11/29/18.
//  Copyright © 2018 AV. All rights reserved.
//

import UIKit

class CoffeeMachine: NSObject {
    var coffeeInMachine = 200
    var waterInMachine = 100
    var milkInMachine = 100
    var result = ""
    
    func recList(choice: Int) -> (resultTag: String,
                                            resultWater: Int,
                                            resultCoffee: Int,
                                            resultMilk: Int) {
            let americano = Recipes(drink: "американо", waterNeeded: 50, coffeNeeded: 50, milkNeeded: 0)
            let latte = Recipes(drink: "латте", waterNeeded: 100, coffeNeeded: 50, milkNeeded: 200)
            let capuccino = Recipes(drink: "капуччино", waterNeeded: 100, coffeNeeded: 50, milkNeeded: 100)
            let recipeList = [americano, capuccino, latte]
            var resultTag = recipeList[choice].drink
            var resultWater = recipeList[choice].waterNeeded
            var resultCoffee = recipeList[choice].coffeeNeeded
            var resultMilk = recipeList[choice].milkNeeded
            return (resultTag, resultWater, resultCoffee, resultMilk)
    }
    
    func makeCoffe(choice: Int) -> String{
        var whatDrink = recList(choice: choice)
        print(makeItIfYouCan(tag: whatDrink.resultTag, water: whatDrink.resultWater, coffeeBeans: whatDrink.resultCoffee, milk: whatDrink.resultMilk))
        return result
    }
    
    func addToMachine(choice: Int) {
        switch choice {
        case 0:
            coffeeInMachine += 100
        case 1:
            waterInMachine += 100
        case 2:
            milkInMachine += 100
        default:
            "Ошибка выполнения"
        }
    }
    
    func makeItIfYouCan(tag: String, water: Int, coffeeBeans: Int, milk: Int) -> String{
        var errorMessege = ""
        
        //Проверка наличия ингридиентов
        if coffeeInMachine < coffeeBeans {
            errorMessege = " зерна"
            result = "Добавьте:\(errorMessege)"
            return result
        } else if waterInMachine < water {
            errorMessege = " воду"
            result = "Добавьте:\(errorMessege)"
            return result
        } else if milkInMachine < milk {
            errorMessege = " молоко"
            result = "Добавьте:\(errorMessege)"
            return result
        }
        else {
            coffeeInMachine = coffeeInMachine - coffeeBeans
            waterInMachine = waterInMachine - water
            milkInMachine = milkInMachine - milk
            result = "Вот ваш \(tag)"
            return result
        }
        
    }
    
}

